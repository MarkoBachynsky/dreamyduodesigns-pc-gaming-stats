
let address       = String(window.location.href).substring(0, String(window.location.href).lastIndexOf('/'));
let json_data     = null;
let map_game_apps = new Map();

// filters for game searching
let array_filter_included_tags         = [];
let array_filter_included_categories   = [];
let array_filter_included_genres       = [];
let array_filter_included_is_free      = [];
let array_filter_included_developers   = [];
let array_filter_included_publishers   = [];
let array_filter_included_platforms    = [];
let array_filter_included_release_date = [];

// filters for game searching
let array_filter_excluded_tags         = [];
let array_filter_excluded_categories   = [];
let array_filter_excluded_genres       = [];
let array_filter_excluded_is_free      = [];
let array_filter_excluded_developers   = [];
let array_filter_excluded_publishers   = [];
let array_filter_excluded_platforms    = [];
let array_filter_excluded_release_date = [];

let map_tags                = new Map();
let map_categories          = new Map();
let map_genres              = new Map();
let map_is_free             = new Map();
let map_developers          = new Map();
let map_publishers          = new Map();
let map_platforms           = new Map();
let map_release_date        = new Map();


// filters for game searching
let array_filters =
[
    ['tags',         map_tags,         array_filter_included_tags,         array_filter_excluded_tags],
    ['categories',   map_categories,   array_filter_included_categories,   array_filter_excluded_categories],
    ['genres',       map_genres,       array_filter_included_genres,       array_filter_excluded_genres],
    ['is_free',      map_is_free,      array_filter_included_is_free,      array_filter_excluded_is_free],
    ['developers',   map_developers,   array_filter_included_developers,   array_filter_excluded_developers],
    ['publishers',   map_publishers,   array_filter_included_publishers,   array_filter_excluded_publishers],
    ['platforms',    map_platforms,    array_filter_included_platforms,    array_filter_excluded_platforms],
    ['release_date', map_release_date, array_filter_included_release_date, array_filter_excluded_release_date]
];

// update_map_game_apps();
update_table();

function modal_apply()
{
    // console.log($("#input_modal").val());
    let app_id          = $('#input_modal').attr('app_id');
    let game_name       = $('#input_modal').attr('game_name');
    let new_reddit_name = $("#input_modal").val();

    fetch(address + '/update_game_app',
    { 
        method: 'PATCH' ,
        headers: { "Content-Type": "application/json", },
        body: JSON.stringify(
        {
            app_id:            $('#input_modal').attr('app_id'),
            new_reddit_name:   new_reddit_name
        })
    })
    .then(response => response.json()) 
    .then(data =>   
    {
        console.log('response: ' + data);
        if (data[0] == true)
        {
            let subreddit_count = data[1];

            // console.log('0000');

            let HTML_modal = 
            `
            <!-- Modal content-->
            <div class='modal-content'>
                <div class='modal-header'>
                    <h4 class='modal-title'>${game_name}</h4>
                </div>
                <div class='modal-body'>
                    <p>Reddit: <a href='https://www.reddit.com/r/${new_reddit_name}' target='_blank'>${new_reddit_name}</a></p>
                    <input type='text' class='form-control' id='input_modal' app_id='${app_id}' game_name='${game_name}' placeholder='subreddit'>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-primary btn-sm rounded-pill' onclick='modal_apply()' >Apply</button>
                    <button type='button' class='btn btn-primary btn-sm rounded-pill' onclick='modal_close()' >Close</button>
                </div>
            </div>
            `;
            document.getElementById('modal_content').innerHTML = HTML_modal;

            // console.log('1111');

            $(`#row_reddit_${app_id}`).attr('subreddit_name',  new_reddit_name);
            $(`#row_reddit_${app_id}`).attr('subreddit_count', subreddit_count);

            // console.log('2222');

            // $(`#row_reddit_${app_id}`).val(subreddit_count.toLocaleString());
            document.getElementById(`row_reddit_${app_id}`).innerHTML = subreddit_count.toLocaleString();

            $("#input_modal").val('');
        }
        else
        {

        }
    });
}

function modal_close()
{
    $('#modal_reddit').modal('hide');
}

function filter_click(event, filter_name) 
{
    let array_filter_element_selection;
    for (element of array_filters)
    {
        if (element[0] === filter_name)
        {
            array_filter_element_selection = element;
        }
    }
    // console.log('array_filter_element_selection');
    // console.log(array_filter_element_selection);
    let array_filter_included_selection = array_filter_element_selection[2];
    let array_filter_excluded_selection = array_filter_element_selection[3];

    console.log(filter_name);
    var event        = event.currentTarget; 
    let filter_value = $(event).attr('filter_value'); 
     
    // console.log('array_filters 1'); 
    // console.log(array_filters);

    // if filter value is not selected, add it 
    if (!array_filter_included_selection.includes(filter_value) && !array_filter_excluded_selection.includes(filter_value)) 
    { 
        // console.log('CASE 1');
        array_filter_included_selection.push(filter_value); 
        array_filter_included_selection.sort();
        console.log(`checkbox_${filter_name}_${filter_value}`);
        document.getElementById(`checkbox_${filter_name}_${filter_value}`).checked = true; 
    }

    // make filter excluded, flip from included
    else if (array_filter_included_selection.includes(filter_value) && !array_filter_excluded_selection.includes(filter_value)) 
    {
        // console.log('CASE 2');
        // console.log('BEFORE:');
        // console.log(array_filter_included_selection);

        array_filter_included_selection = array_filter_included_selection.filter(element => element !== filter_value); 
        // console.log('AFTER:');
        // console.log(array_filter_included_selection);

        array_filter_excluded_selection.push(filter_value); 
        array_filter_excluded_selection.sort();

        console.log(`checkbox_${filter_name}_${filter_value}`);

        document.getElementById(`checkbox_${filter_name}_${filter_value}`).checked = true; 
    }

    // if filter value is selected, remove it 
    else if (array_filter_excluded_selection.includes(filter_value))
    {
        // console.log('CASE 3');
        array_filter_excluded_selection = array_filter_excluded_selection.filter(element => element !== filter_value); 

        document.getElementById(`checkbox_${filter_name}_${filter_value}`).checked = false; 
    } 

    array_filter_element_selection[2] = array_filter_included_selection;
    array_filter_element_selection[3] = array_filter_excluded_selection;

    console.log('array_filters AFTER CLICK');
    console.log(array_filters);
    update_table();
} 

function modal_click(event)
{
    var event       = event.currentTarget;
    let app_id = $(event).attr('app_id');
    let game_name = $(event).attr('game_name');
    let subreddit_name = $(event).attr('subreddit_name'); 
    
    let HTML_modal = 
    `
    <!-- Modal content-->
    <div class='modal-content'>
        <div class='modal-header'>
            <h4 class='modal-title'>${game_name}</h4>
        </div>
        <div class='modal-body'>
            <p>Reddit: <a href='https://www.reddit.com/r/${subreddit_name}' target='_blank'>${subreddit_name}</a></p>
            <input type='text' class='form-control' id='input_modal' app_id='${app_id}' game_name='${game_name}' placeholder='subreddit'>
            </div>
        <div class='modal-footer'>
            <button type='button' class='btn btn-primary btn-sm rounded-pill' onclick='modal_apply()' >Apply</button>
            <button type='button' class='btn btn-primary btn-sm rounded-pill' onclick='modal_close()' >Close</button>
        </div>
    </div>
    `;
    
    document.getElementById('modal_content').innerHTML = HTML_modal;
    $('#modal_reddit').modal('show');
}

async function update_table()
{
    console.log('update_table START')
    map_tags.clear();
    map_categories.clear();
    map_genres.clear();
    map_is_free.clear();
    map_developers.clear();
    map_publishers.clear();
    map_platforms.clear();
    map_release_date.clear();
    // console.log('array_filters 1'); 
    // console.log(array_filters);

    let array_release_date      = [];
    let array_apps_missing_info = [];

    // while (true)
    // {
        // load map_game_apps, build table and filters
        fetch('../json/map_game_apps.json')
        .then(response => response.json())
        .then((data) =>
        {
            console.log('\n\n=============================================');
            console.log(data);
            map_game_apps = new Map([...data].sort((int_key1, int_key2) => int_key2[1].subreddit_count - int_key1[1].subreddit_count));
            let HTML_table = '';
            let index = 1;

            table_loop:
            for (let [key, value] of map_game_apps.entries())
            {
                // console.log(value.name);

                // Filter out games by included and excluded selections - Start
                // Included Filters Selection
                for (let array_filter_name_and_array of array_filters)
                {
                    let string_filter_name    = array_filter_name_and_array[0];
                    let array_filter_included = array_filter_name_and_array[2];

                    // include games with these selected included choices
                    if (array_filter_included.length > 0)
                    {
                        if (Object.hasOwn(value, string_filter_name))
                        {
                            console.log(value);
                            console.log(string_filter_name);
                            console.log('Object.hasOwn(value, string_filter_name TRUE');
                            let bool_all_filters_included = true;
                            // map_filter contains user's desired filter game must have
                            let map_filter = new Map(); // ('Multiplayer', false), will be set to true if found
                            // create checklist of included choices needed to be found
                            array_filter_included.forEach(element =>
                            {
                                map_filter.set(element, false);
                            });
                            // see if all desired included choices exist in game
                            value[string_filter_name].forEach(element =>
                            {
                                console.log('game filter elements');
                                console.log(element);
                                // if (string_filter_name === 'categories' || string_filter_name === 'genres')
                                // {
                                //     console.log('game filter categories/generes');
                                //     console.log(element.description);
                                //     if (array_filter_included.includes(element.description))
                                //     {
                                //         map_filter.set(element.description, true);
                                //     }
                                // }
                                // else
                                // {
                                    if (array_filter_included.includes(element))
                                    {
                                        map_filter.set(element, true);
                                    }
                                // }
                            });
                            // if any desired tags does not exist in game, exclude game from table
                            for (let [filter_key, filter_value] of map_filter.entries())
                            {
                                if (filter_value === false) bool_all_filters_included = false;
                            }
                            if (bool_all_filters_included === false) continue table_loop;
                        }
                        else
                        {
                            console.log('Object.hasOwn(value, string_filter_name FALSE');
                            continue table_loop;
                        }
                    }
                }

                // Excluded Filters Selection
                for (let array_filter_name_and_array of array_filters)
                {
                    let string_filter_name  = array_filter_name_and_array[0];
                    let array_filter_excluded = array_filter_name_and_array[3];
                    if (array_filter_excluded.length > 0)
                    {
                        for (element of value[string_filter_name])
                        {
                            if (array_filter_excluded.includes(element))
                            {
                                continue table_loop;
                            }
                        };
                    }
                }

                // Filter out games by included and excluded selections - End
                
                // Create table row element of game
                let subreddit_name = value.subreddit_name ? value.subreddit_name : '';
                let subreddit_count = value.subreddit_count ? value.subreddit_count : 0;

                HTML_table +=
                `
                <!-- Row -->
                <tr>
                    <!-- Cell Index -->
                    <td class="flex-item" style='color: white; font-weight: bold;'>
                        ${index}
                    </td>

                    <!-- Cell Image -->
                    <td class="flex-item" style='padding: 0px;'>
                        <a href='https://store.steampowered.com/app/${key}' target='_blank'>
                            <img id='img_icon${index}' class='img-thumbnail' src='https://cdn.cloudflare.steamstatic.com/steam/apps/${key}/header.jpg' alt='${value.name} Thumbnail' >
                        </a>
                        <!-- <video id='img_video${index}' class='img-thumbnail' src='https://cdn.cloudflare.steamstatic.com/steam/apps/${value.movies[0]}/microtrailer.webm' alt='${value.name} Trailer'  autoplay='' loop='' ></video> -->
                    </td>

                    <!-- Cell Name' -->
                    <td class="flex-item" style='color: white;'>
                        ${value.name}
                    </td>

                    <!-- Cell Steam -->
                    <td class="flex-item" style='color: #a3cf06;'>
                        ${abbrNum(value.steam_count, 1)}
                    </td>

                    <!-- Cell Reddit -->
                    <td class="flex-item" id='row_reddit_${key}' app_id='${key}' game_name='${value.name}' subreddit_name='${subreddit_name}' subreddit_name='${value.name}' style='color: rgb(254, 63, 0);' onclick='modal_click(event)' >
                        ${abbrNum(subreddit_count, 1)}
                    </td>
                </tr>
                `;

                // Add up games tags, categories, etc. to filter count
                try
                {
                    value.tags.forEach(element =>
                        {
                            if (map_tags.has(element))
                            {
                                let new_element       = map_tags.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_tags.set(new_element.description, new_element);
                            }
                    });
                    if (value.tags.length < 20) array_apps_missing_info.push([value.name, 'missing: < 20 tags']);
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: tags']); }

                // console.log(value.categories);
                try
                {
                    value.categories.forEach(element =>
                        {
                            if (map_categories.has(element))
                            {
                                let new_element       = map_categories.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_categories.set(new_element.description, new_element);
                            }
                    });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: categories']); }

                // console.log(value.genres);
                try
                {
                    value.genres.forEach(element =>
                        {
                            if (map_genres.has(element))
                            {
                                let new_element       = map_genres.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_genres.set(new_element.description, new_element);
                            }
                    });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: genres']); }
                
                // console.log(value.is_free);
                try
                {
                    value.is_free.forEach(element =>
                        {
                            if (map_is_free.has(element))
                            {
                                let new_element       = map_is_free.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_is_free.set(new_element.description, new_element);
                            }
                    });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: is_free']); }

                // console.log(value.developers);
                try
                {
                    value.developers.forEach(element =>
                    {
                        if (map_developers.has(element))
                        {
                            let new_element       = map_developers.get(element);
                            new_element.quantity += 1;
                        }
                        else
                        {
                            let new_element = {};
                            new_element.description = element;
                            new_element.quantity    = 1;
                            map_developers.set(new_element.description, new_element);
                        }
                });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: developers']); }
                
                // console.log(value.publishers);
                try
                {
                    value.publishers.forEach(element =>
                    {
                        if (map_publishers.has(element))
                        {
                            let new_element       = map_publishers.get(element);
                            new_element.quantity += 1;
                        }
                        else
                        {
                            let new_element = {};
                            new_element.description = element;
                            new_element.quantity    = 1;
                            map_publishers.set(new_element.description, new_element);
                        }
                });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: publishers']); }
                
                // console.log(value.platforms);
                try
                {
                    value.platforms.forEach(element =>
                        {
                            if (map_platforms.has(element))
                            {
                                let new_element       = map_platforms.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_platforms.set(new_element.description, new_element);
                            }
                    });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: platforms']); }
                
                // console.log(value.release_date);
                try
                {
                    value.release_date.forEach(element =>
                        {
                            if (map_release_date.has(element))
                            {
                                let new_element       = map_release_date.get(element);
                                new_element.quantity += 1;
                            }
                            else
                            {
                                let new_element = {};
                                new_element.description = element;
                                new_element.quantity    = 1;
                                map_release_date.set(new_element.description, new_element);
                            }
                    });
                } catch (error) { if (!array_apps_missing_info.includes(value.name)) array_apps_missing_info.push([value.name, 'missing: release_date']); }
                
                // console.log('=================================');

                // if (index >= 3)
                //     break table_loop;

                index++;
            }
            array_apps_missing_info.sort();
            console.log('array_apps_missing_info');
            console.log(array_apps_missing_info);

            // Build Filter Sidebar - Start
            for (let element of array_filters)
            {
                let filter_name                = element[0];
                let filter_map                 = element[1];
                let array_filter_included      = element[2];
                let array_filter_excluded      = element[3];
                let array_filter_included_zero = array_filter_included.slice();

                console.log('filter_map');
                console.log(filter_map);
                // console.log('array_filter_excluded');
                // console.log(array_filter_excluded);

                // filter_map
                if (filter_name === 'release_date')
                    filter_map = new Map([...filter_map].sort((array_value1, array_value2) => array_value1[1].description - array_value2[1].description)); // Sort by integer key value ascending
                else
                    filter_map = new Map([...filter_map].sort((array_value1, array_value2) => array_value2[1].quantity -  array_value1[1].quantity)); // Sort by integer key value ascending

                // console.log('filter_map');
                // console.log(filter_map);
                let HTML_filter_searchbar = `<input type="text" class="form-control" placeholder='Search' style="margin-bottom: 6px;" aria-label="Text input with checkbox">`;
                let HTML_filter_excluded = '';
                let HTML_filter_included = '';
                let HTML_filter_unselected = '';

                for (sub_element of array_filter_excluded)
                { 
                    HTML_filter_excluded +=
                    `
                        <!-- Item -->
                        <div class="input-group filter-items"  style="display: flex; margin-bottom: 6px;" onclick="filter_click(event, '${filter_name}')" filter_value='${sub_element}'> 
                            <div class='' style=" display: flex; position: relative;  white-space: nowrap; overflow: hidden; text-overflow: ellipsis; flex-grow: 1;">
                
                                <!-- Input Group Left  -->
                                <div class="input-group-prepend" style="display: flex;">
                                    <div style="background-color: rgba(0,0,0,0); border: none; display: flex; padding: 3px;">
                                        <input id='checkbox_${filter_name}_${sub_element}' type="checkbox" checked=true aria-label="Checkbox for following text input">
                                    </div>
                                </div>
                
                                <!-- Input Group Middle  -->
                                <div style="display: flex; text-decoration: line-through;">
                                    <div>${sub_element}</div>
                                </div>
                
                            </div>
                            <!-- Input Group Right  -->
                            <div class="input-group-append" style="display: flex; font-size: 12px; font-weight: 700; color: var(--body-color); background-color: var(--muted-bg-color); border-radius: 10px; font-variant-numeric: tabular-nums;">
                                <div>
                                    <span class="badge badge-pill badge-dark">X</span>
                                </div>
                            </div>
                        </div>
                    `;
                }

                for (let [key, sub_element] of filter_map.entries())
                { 
                    if (array_filter_included.includes(sub_element.description))
                    {
                        HTML_filter_included +=
                        `
                            <!-- Item -->
                            <div class="input-group filter-items"  style="display: flex; margin-bottom: 6px;" onclick="filter_click(event, '${filter_name}')" filter_value='${sub_element.description}'> 
                                <div class='' style=" display: flex; position: relative;  white-space: nowrap; overflow: hidden; text-overflow: ellipsis; flex-grow: 1;">
                    
                                    <!-- Input Group Left  -->
                                    <div class="input-group-prepend" style="display: flex;">
                                        <div style="background-color: rgba(0,0,0,0); border: none; display: flex; padding: 3px;">
                                            <input id='checkbox_${filter_name}_${sub_element.description}' type="checkbox" checked=true aria-label="Checkbox for following text input">
                                        </div>
                                    </div>
                    
                                    <!-- Input Group Middle  -->
                                    <div style="display: flex;">
                                        <div>${sub_element.description}</div>
                                    </div>
                    
                                </div>
                                <!-- Input Group Right  -->
                                <div class="input-group-append" style="display: flex; font-size: 12px; font-weight: 700; color: var(--body-color); background-color: var(--muted-bg-color); border-radius: 10px; font-variant-numeric: tabular-nums;">
                                    <div>
                                        <span class="badge badge-pill badge-dark">${sub_element.quantity.toLocaleString()}</span>
                                    </div>
                                </div>
                            </div>
                        `;
                        array_filter_included_zero = array_filter_included_zero.filter(element_filter_name => element_filter_name !== sub_element.description);
                    }
                }

                for (let sub_element of array_filter_included_zero)
                {
                    HTML_filter_included +=
                    `
                        <!-- Item -->
                        <div class="input-group filter-items"  style="display: flex; margin-bottom: 6px;" onclick="filter_click(event, '${filter_name}')" filter_value='${sub_element}'> 
                            <div class='' style=" display: flex; position: relative;  white-space: nowrap; overflow: hidden; text-overflow: ellipsis; flex-grow: 1;">
                
                                <!-- Input Group Left  -->
                                <div class="input-group-prepend" style="display: flex;">
                                    <div style="background-color: rgba(0,0,0,0); border: none; display: flex; padding: 3px;">
                                        <input id='checkbox_${filter_name}_${sub_element}' type="checkbox" checked=true aria-label="Checkbox for following text input">
                                    </div>
                                </div>
                
                                <!-- Input Group Middle  -->
                                <div style="display: flex;">
                                    <div>${sub_element}</div>
                                </div>
                
                            </div>
                            <!-- Input Group Right  -->
                            <div class="input-group-append" style="display: flex; font-size: 12px; font-weight: 700; color: var(--body-color); background-color: var(--muted-bg-color); border-radius: 10px; font-variant-numeric: tabular-nums;">
                                <div>
                                    <span class="badge badge-pill badge-dark">0</span>
                                </div>
                            </div>
                        </div>
                    `;
                }

                for (let [key, sub_element] of filter_map.entries())
                { 
                    // console.log(sub_element);
                    if (sub_element.description.length === 0)
                    {
                        console.log('SKIPPED - ' + filter_name + ' : (' + sub_element.description + ')');
                        continue;
                    }
                    if (!array_filter_included.includes(sub_element.description))
                    {
                        // console.log(sub_element);
                        HTML_filter_unselected +=
                        `
                            <!-- Item -->
                            <div class="input-group filter-items"  style="display: flex; margin-bottom: 6px;" onclick="filter_click(event, '${filter_name}')" filter_value='${sub_element.description}'> 
                                <div class='' style=" display: flex; position: relative;  white-space: nowrap; overflow: hidden; text-overflow: ellipsis; flex-grow: 1;">
                    
                                    <!-- Input Group Left  -->
                                    <div class="input-group-prepend" style="display: flex;">
                                        <div style="background-color: rgba(0,0,0,0); border: none; display: flex; padding: 0px 3px 3px 3px;">
                                            <input id='checkbox_${filter_name}_${sub_element.description}' type="checkbox" aria-label="Checkbox for following text input">
                                        </div>
                                    </div>
                    
                                    <!-- Input Group Middle  -->
                                    <div style="display: flex;">
                                        <div>${sub_element.description}</div>
                                    </div>
                    
                                </div>
                                <!-- Input Group Right  -->
                                <div class="input-group-append" style="display: flex; font-size: 12px; font-weight: 700; color: var(--body-color); background-color: var(--muted-bg-color); border-radius: 10px; font-variant-numeric: tabular-nums;">
                                    <div>
                                        <span class="badge badge-pill badge-dark">${sub_element.quantity.toLocaleString()}</span>
                                    </div>
                                </div>
                            </div>
                        `;
                    }
            };
            let HTML = HTML_filter_searchbar + HTML_filter_excluded + HTML_filter_included + HTML_filter_unselected;
            // apply HTML to filter
            document.getElementById(`div_filter_${filter_name}_items`).innerHTML = HTML;
            }

            // apply HTML to table
            document.getElementById('table_games').innerHTML = HTML_table;

            // Build Filter Sidebar - End
            console.log('Table updated')

            console.log('array_filters 2'); 
            console.log(array_filters);  
        });

        // update_img_slideshow();
    //     await new Promise(resolve => setTimeout(resolve, 2000)); // wait 2.0 seconds
    // }
}

async function update_img_slideshow()
{
    while (true)
    {
        for (let [key, value] of map_game_apps.entries())
        {
            screenshots.forEach(screenshot =>
                {
                    screenshot.path_thumbnail = screenshot.path_thumbnail.split('?')[0];
                    screenshot.path_full      = screenshot.path_full.split('?')[0];
                });
        }
        await new Promise(resolve => setTimeout(resolve, 2000)); // wait 2.0 seconds
        update_img_slideshow();
    }
}

function get_cookie(string)
{
    return document.cookie
    .split('; ')
    .find((row) => row.startsWith(string))
    ?.split('=')[1];
}

function alertNotify(message, alertType, iconChoice, duration) 
{ 
    if (iconChoice == 1)      // ✔ 
        iconChoice = 'check-circle-fill'; 
    else if (iconChoice == 2) // i 
        iconChoice = 'info-fill'; 
    else if (iconChoice == 3) // ! 
        iconChoice = 'exclamation-triangle-fill'; 
 
    let iconHTML = `<svg class='bi flex-shrink-0 me-2' width='24' height='24' role='img' aria-label='${alertType}}:'><use xlink:href='#${iconChoice}'/></svg>`; 
    alertType = `alert-${alertType}`; 
 
    let html =  
    ` 
    <div id='alertNotification' class='alert ${alertType}  text-center  col-auto' style='margin: 0 auto; align-text: center;' role='alert'> 
        <span> 
            ${iconHTML} 
            ${message} 
        </span> 
    </div> 
    `; 
 
    // show pop up 
    $('#notification').append(html); 
     
    duration *= 1000; 
    setTimeout(function () 
    { // this will automatically close the alert in 2 secs 
        $('#alertNotification').remove(); 
    }, duration); 
}

function abbrNum(number, decPlaces)
{
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "k", "m", "b", "t" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--)
    {
        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number)
        {
             // Here, we multiply by decPlaces, round, and then divide by decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next abbreviation
             if((number == 1000) && (i < abbrev.length - 1))
             {
                 number = 1;
                 i++;
             }
             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }

    return number;
}

function mapAwareReplacer(key, value)
{
    if (value instanceof Map && typeof value.toJSON !== "function") {
        return [...value.entries()];
    }
    return value;
}

function onclick_filter(event)
{
    var event        = event.currentTarget;
    let filter_value = $(event).text();
    if (filter_value.includes('▽')) filter_value = filter_value.replace('▽', '▷');
    else if (filter_value.includes('▷')) filter_value = filter_value.replace('▷', '▽');
    $(event).text(filter_value);
}
