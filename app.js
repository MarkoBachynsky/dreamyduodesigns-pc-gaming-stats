// var bootstrap;

const CORS = require( 'cors' );
var express = require( 'express' );
var session = require( 'express-session' );

var path = require( 'path' );
var dotenv = require( 'dotenv' );
dotenv.config();
const cookieParser = require( 'cookie-parser' );
var FS = require( 'fs' );
const JSEARCH = require('jsearch');
let STRING_SIMILARITY = require('string-similarity');


const SCHEDULE = require('node-schedule');

// select DOM
const CHEERIO = require('cheerio');
// get DOM from URL
const AXIOS = require('axios'); 
AXIOS.default.withCredentials = true; // allow sending cookies with requests


const SNOOWRAP = require('snoowrap');
// STREAM API
// You are limited to one hundred thousand (100,000) calls to the Steam Web API per day. 
// https://steamcommunity.com/dev/apiterms
const STEAM_API = require( 'steamapi' );
const STEAM = new STEAM_API( process.env.Steam_Web_API_Key );

// REDDIT API 
const REDDIT_API = new SNOOWRAP({ 
    userAgent:    'Jango-Gaming-Pop-Tracker_(by_/u/JangoBingoBango)', 
    clientId:     'g4EUth2LB8ECvncCCkMntA', 
    clientSecret: 'y5FVMPDIKIb21n7Qsg11ngeV_ZQ4dg', 
    username:     'JangoBingoBango', 
    password:     'UOu6z2Lh3Yj5SaFh82Fh' 
  }); 


// debuging / logging
let STRING_FOLDER_NAME = `logs/${new Date().toISOString().split('T')[0]}`;
// FS.mkdirSync(STRING_FOLDER_NAME);
FS.mkdirSync(STRING_FOLDER_NAME, { recursive: true });
FS.mkdirSync('public/json/google_trends', { recursive: true });

let ARRAY_FILE_NAMES = FS.readdirSync(STRING_FOLDER_NAME);
let STRING_FILE_NAME = `${ARRAY_FILE_NAMES.length}_debug`;
// Write data in 'Output.txt' .
let STRING_DIR_LOGS = `${STRING_FOLDER_NAME}/${STRING_FILE_NAME}.log`;
let is_updating = false;

// express object 
let app = express();
app.use( express.json() );
app.use( express.urlencoded( { extended: false } ) );

// set public folder as root 
app.use( express.static( path.join( __dirname, 'public' ) ) );

// setup Cross-Origin Resource Sharing (CORS) 
// origin: allow data from anywhere 
// credentials: allow cookies 

// https://www.section.io/engineering-education/session-management-in-nodejs-using-expressjs-and-express-session/

app.use(
    CORS(
        {
            origin: '*',
            credentials: true
        }
    )
);

// session config
app.use( session(
    {
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    } ) );

// setup cookieparser 
app.use( cookieParser() );

// no ETag
app.set( 'etag', false );

// view engine setup 
app.set( 'views', path.join( __dirname, '/public/views' ) );
app.set( 'view engine', 'ejs' );

// website root index route  
app.get( '/', ( request, response ) => 
{
    write_log('route(/)');
    // read cookies 
    // Cookies that have not been signed 
    write_log( 'Cookies: ', request.cookies );

    // https://www.youtube.com/watch?v=gdn9B0LCiI4

    // Cookies that have been signed 
    write_log( 'Signed Cookies: ', request.signedCookies );

    // bootstrap = require('bootstrap');
    response.redirect( 'steam' );

} );

// render 
app.get( '/services', ( request, response ) =>
{
    write_log('route(/services)' );
    response.render( 'services' );
} );

// render 
app.get( '/stickers', ( request, response ) => 
{
    write_log('route(/stickers)' );
    response.render( 'stickers' );
} );

// render 
app.get( '/steam', ( request, response ) =>
{
    write_log('route(/steam)' );
    response.render( 'steam' );
});

// Steam API - Create Map
async function steam_create_map() 
{
    is_updating = true;
    /*
    compare new api map list with current map list
    only get_reddit() for new items
    */
   
    // update the steam app list
    const promise_steam_saved_apps_updated = new Promise((resolve, reject) =>
    {
        let date_today              = new Date();
        write_log('date_today: ' + date_today.toISOString());

        let json_config = {};
        json_config.Date_Apps_Last_Updated = new Date('2000-01-01T01:00:00');
        try
        {
            json_config = JSON.parse(FS.readFileSync( './public/json/config.json', 'utf-8' ));
        } catch (error) {}

        let bool_is_today            = (date_today.toDateString() == new Date(json_config.Date_Apps_Last_Updated).toDateString());
        // if the map hasn't been updated in the past 24 hours, do so
        if ( bool_is_today == false )
        {
            write_log('Time to update app list.');
            STEAM.getAppList()
            .then( async array_steam_api_apps =>
            {
                write_log('getAppList start');
                // write_log(array_steam_api_apps);
                write_log('array_steam_api_apps.length: ' +  array_steam_api_apps.length);

                let new_map_steam_api_apps = new Map();
                // loop through new apps, and sort it in ascending order
                for (let i = 0; i < Object.keys( array_steam_api_apps ).length; i++)
                {
                    // add app to map_steam_api_apps
                    let app = array_steam_api_apps[i];
                    new_map_steam_api_apps.set(app.appid, '');
                }
                write_log('new_map_steam_api_apps created');
    
                // Sort by int_key value, ascending order
                new_map_steam_api_apps = new Map([...new_map_steam_api_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0]));
                write_log('new_map_steam_api_apps sorted');
    
                // update backup map_steam_saved_apps.json
                FS.writeFile( './public/json/map_steam_saved_apps.json', JSON.stringify( new_map_steam_api_apps, mapAwareReplacer ), 'utf-8', ( error ) =>
                {
                    if ( error ) 
                    {
                        reject(error);
                    }
                    else
                    {
                        write_log( 'public/json/map_steam_saved_apps.json updated!' );
    
                        // update config var: Date_Apps_Last_Updated
                        let json_config = {};
                        try
                        {
                            json_config = JSON.parse(FS.readFileSync( './public/json/config.json', 'utf-8' ));
                        }
                        catch (error) {}

                        json_config.Date_Apps_Last_Updated = new Date().toISOString();
                    
                        FS.writeFile( './public/json/config.json', JSON.stringify( json_config ), 'utf-8', ( error ) =>
                        {
                            if ( error ) 
                            {
                                reject(error);
                            }
                            else
                            {
                                write_log( 'public/json/config.json updated!' );
                                resolve(new_map_steam_api_apps);
                            }
                        });
                    }
                });
            });
        }
        else
        {
            let map_steam_saved_apps  = new Map(JSON.parse(FS.readFileSync( './public/json/map_steam_saved_apps.json','utf-8' )));
            resolve(map_steam_saved_apps);
        }
        
    }).catch( ( error ) =>
    {
        write_log( error );
        is_updating = false;
        return error; 
    });

    // go through the steam app list, sort into game_data list, low_players list, and ignore_apps list
    promise_steam_saved_apps_updated.then(async (map_steam_api_apps) =>
    {
        write_log('promise_steam_saved_apps_updated');
        // write_log(value);

        // 100,000 requests/day, 4,166.67 requests/hour,  69 requests/minute, 1 requests/seconds
        let map_game_apps, map_error_apps, map_low_players_apps, map_ignore_apps; 
        try { map_game_apps        = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json',        'utf-8' ))); } catch (error) { map_game_apps        = new Map(); }
        try { map_error_apps       = new Map(JSON.parse(FS.readFileSync( './public/json/map_error_apps.json',       'utf-8' ))); } catch (error) { map_error_apps       = new Map(); }

        try { map_low_players_apps = new Map(JSON.parse(FS.readFileSync( './public/json/map_low_players_apps.json', 'utf-8' ))); } catch (error) { map_low_players_apps = new Map(); }
        try { map_ignore_apps      = new Map(JSON.parse(FS.readFileSync( './public/json/map_ignore_apps.json',      'utf-8' ))); } catch (error) { map_ignore_apps      = new Map(); }

        let i = 0;
        loopdeloop:
        for (let [key, value] of map_steam_api_apps.entries())
        {
            // write_log( '\n\n#' + i + '\t' + 'key: ' + key + "\t" + 'appname: ' + app.name);
            if (!map_game_apps.has(key) && !map_error_apps.has(key) && !map_low_players_apps.has(key) && !map_ignore_apps.has(key)) // if app hasn't already been checked...
            {
                // await new Promise(resolve => setTimeout(resolve, 100));
                await new Promise(resolve => setTimeout(resolve, 2400)); // wait 2.4 seconds

                // await new Promise(resolve => setTimeout(resolve, 4000)); // wait 2.4 seconds
                // await new Promise(resolve => setTimeout(resolve, 4800)); // wait 2.4 seconds

                // if app type is game, save it
                STEAM.getGameDetails(key).then(async (game_details) =>
                {
                    write_log('game_details:');
                    write_log(`App(${key}) Name: ` + game_details.name);

                    if (game_details.type = 'game')
                    {
                        write_log(`App(${key}) game_details.type = 'game'`);
                        // write_log(game_details);

                        // write_log(game_details.genres);

                        // await new Promise(resolve => setTimeout(resolve, 1200)); // wait 2.4 seconds
                        // await new Promise(resolve => setTimeout(resolve, 1)); // wait 2.4 seconds

                        STEAM.getGamePlayers(key)
                        .then(async player_count =>
                        {
                            write_log(`App(${key}) Player_count: ` + player_count);
                            // write_log(`\n#${i} \t App(${key}) \t name: ${app.name} \t playercount: ${player_count}`);
                            if (player_count >= 100)
                            {
                                write_log(`App(${key}) if (player_count >= 100)`);

                                // remove unnecessary text
                                // game_details.screenshots.forEach(screenshot =>
                                // {
                                //     delete screenshot.path_thumbnail;
                                //     screenshot.path_full      = screenshot.path_full.split('?')[0];
                                // });
                                let movies = [];

                                try
                                {
                                    game_details.movies.forEach(movie =>
                                    {
                                        movies.push(movie.id);
                                    });
                                } catch (error)
                                {
                                    write_log(`App(${key}): No movies`);    
                                }


                                // get subreddit
                                get_subreddit(game_details.name).then((array_subredit_name_and_subreddit_count) =>
                                {
                                    get_tags(key).then((array_tags) =>
                                    {
                                        try
                                        {
                                            if (array_tags.length < 20)
                                            {
                                                write_log('\n\n\n\n====================================');

                                                write_log('Case 1 BREAK: LESS THAN 20 TAGS');
                                                write_log('array_tags');
                                                write_log(array_tags);
                                                write_log('====================================\n');
                                            }
                                            // write_log(`Case 1: ${key}`);
                                            

                                            // platforms
                                            let platforms = [];
                                            Object.entries(game_details.platforms).forEach(([key, element]) => 
                                            {
                                                // key, element: { windows: true, linux: false }
                                                if (element === true)
                                                {
                                                    platforms.push(key);
                                                }
                                            });

                                            let categories = [];
                                            // categories
                                            Object.entries(game_details.categories).forEach(([key, element]) => 
                                            {
                                                categories.push(element.description);
                                            });

                                            let genres = [];
                                            // genres
                                            Object.entries(game_details.genres).forEach(([key, element]) => 
                                            {
                                                genres.push(element.description);
                                            });
                                            
                                            // is_free
                                            let is_free = [];
                                            is_free.push(game_details.is_free);

                                            write_log('game_details.release_date');
                                            write_log(JSON.stringify(game_details.release_date));
                                            // release_date
                                            let release_date = [];
                                            if (game_details.release_date.date.length == 0)
                                                release_date.push('2000');
                                            else if (game_details.release_date.date.includes(','))
                                                release_date.push(game_details.release_date.date.split(', ')[1]);
                                            else
                                                release_date.push(game_details.release_date.date.split(' ')[1]);

                                            // sort
                                            platforms.sort();
                                            categories.sort();
                                            genres.sort();
                                            is_free.sort();
                                            game_details.developers.sort();
                                            game_details.publishers.sort();
                                            release_date.sort();
                                            array_tags.sort();

                                            // save game_app with reddit
                                            map_game_apps.set( key, 
                                            { 
                                                name:                       game_details.name, 
                                                header_image:               game_details.header_image.split('?')[0],
                                                // screenshots:                game_details.screenshots,
                                                platforms:                  platforms,
                                                categories:                 categories,
                                                genres:                     genres,
                                                is_free:                    is_free,
                                                developers:                 game_details.developers,
                                                publishers:                 game_details.publishers,
                                                release_date:               release_date,
                                                movies:                     movies,
                                                steam_count:                player_count, 
                                                date_steam_count_updated:   new Date().toISOString(),
                                                tags:                       array_tags
                                            });
        
                                            map_game_apps = new Map([...map_game_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                                            FS.writeFile( './public/json/map_game_apps.json', JSON.stringify( map_game_apps, mapAwareReplacer ), 'utf-8', ( error1 ) =>
                                            {
                                                if ( error1 ) 
                                                {
                                                    throw error1;
                                                }
                                                else
                                                {
                                                    write_log(`Case 0: ${key}`);
                                                    // write_log( 'map_game_apps.json updated!' );
                                                }
                                            });
                                        }
                                        catch (error)
                                        {
                                            write_log(error);
                                            write_log(`Error: App(${key}) added to map_error_apps.json ` + game_details.name + ': ' + error);
                                            map_error_apps.set(key, game_details.name);
                                            FS.writeFile( './public/json/map_error_apps.json', JSON.stringify( map_error_apps, mapAwareReplacer ), 'utf-8', ( error1 ) =>
                                            {
                                                if ( error1 ) 
                                                {
                                                    write_log(`Case ERROR WTF map_error_apps: ${key}`);
                                                    throw error1;
                                                }
                                                else
                                                {
                                                    write_log('map_error_apps updated\n\n\n\n');
                                                }
                                            });
                                        }

                                    }).catch((error) =>
                                    {
                                        write_log(`Case 1: ${key}`);
                                        write_log(error);
                                    });
                                })
                                .catch((error) =>
                                {
                                    write_log(`Case 2: ${key}`);
                                    write_log(error);
                                    map_error_apps.set(key, game_details.name);
                                    FS.writeFile( './public/json/map_error_apps.json', JSON.stringify( map_error_apps, mapAwareReplacer ), 'utf-8', ( error1 ) =>
                                    {
                                        if ( error1 ) 
                                        {
                                            write_log(`Case ERROR WTF map_error_apps: ${key}`);
                                            throw error1;
                                        }
                                        else
                                        {
                                            write_log('map_error_apps updated\n\n\n\n');
                                        }
                                    });
                                });
                            }
                            else
                            {   
                                write_log(`Case 3 Low Players: ${key}`);
                                map_low_players_apps.set( key, '');
                                map_low_players_apps = new Map([...map_low_players_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                                FS.writeFile( './public/json/map_low_players_apps.json', JSON.stringify( map_low_players_apps, mapAwareReplacer), 'utf-8', ( error1 ) =>
                                {
                                    if ( error1 )
                                    {
                                        throw error1;
                                    }
                                    else
                                    {
                                        // write_log( 'map_low_players_apps.json updated!' );
                                    }
                                } );
                            }
                        })
                        .catch((error) =>
                        {
                            write_log(`Case 4: ${key}`);
                            write_log(`Not found`);
                            write_log(error);
                            map_low_players_apps.set( key, '');
                            map_low_players_apps = new Map([...map_low_players_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                            FS.writeFile( './public/json/map_low_players_apps.json', JSON.stringify( map_low_players_apps, mapAwareReplacer), 'utf-8', ( error1 ) =>
                            {
                                if ( error1 )
                                {
                                    throw error1;
                                }
                                else
                                {
                                    // write_log( 'map_low_players_apps.json updated!' );
                                }
                            } );
                        });
                    }
                    else
                    {
                        write_log(`Case 5: ${key}`);
                        map_ignore_apps.set( key, '');
                        map_ignore_apps = new Map([...map_ignore_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                        FS.writeFile( './public/json/map_ignore_apps.json', JSON.stringify( map_ignore_apps, mapAwareReplacer), 'utf-8', ( error1 ) =>
                        {
                            if ( error1 )
                            {
                                throw error1;
                            }
                            else
                            {
                                // write_log( 'map_ignore_apps.json updated!' );
                            }
                        } );
                    }
                })
                .catch(error =>
                {
                    write_log(`Case 6: ${key}`);
                    if (error.toString().toLowerCase().includes('found') || error.toString().toLowerCase().includes('invalid')) write_log(`App(${key}) not found`);
                    else write_log(error);
        
                    map_ignore_apps.set( key, '');
                    map_ignore_apps = new Map([...map_ignore_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                    FS.writeFile( './public/json/map_ignore_apps.json', JSON.stringify( map_ignore_apps, mapAwareReplacer), 'utf-8', ( error1 ) =>
                    {
                        if ( error1 )
                        {
                            throw error1;
                        }
                        else
                        {
                            // write_log( 'map_ignore_apps.json updated!' );
                        }
                    } );
                });
            // if (i > 1)
            //     break loopdeloop;
            // i++;
            }
        }

        is_updating = false;
        write_log('COMPLETE');
    })
    .catch((error) =>
    {
        write_log('promise_steam_saved_apps_updated ERROR');
        is_updating = false;
        write_log(error);
    });
}

// Update Map Game Apps
app.patch( '/update_map_game_apps', async ( request, response ) =>
{
    write_log('/app.patch( \'/update_map_game_apps\')');

    let map_updated_game_apps = new Map(JSON.parse(request.body.map_updated_game_apps));

    try
    { 
        let map_game_apps = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' ))); 

        for (let [key, value] of map_updated_game_apps.entries())
        {
            map_game_apps.set(key, value);
        }

        FS.writeFile( './public/json/map_game_apps.json', JSON.stringify( map_game_apps, mapAwareReplacer ), 'utf-8', ( error ) =>
        {
            if ( error ) 
            {
                throw error;
            }
            else
            {
                // write_log( 'map_game_apps.json updated!' );
                response.json(true);
            }
        } );
    }
    catch (error)
    { 
        write_log(error);
        response.json(false);
    }
});

// update steam_count for map_game_apps
function update_steam_count() 
{
    let promise_update_steam_count = new Promise((resolve, reject) =>
    {
        let map_game_apps, map_low_players_apps; 

        try { map_game_apps        = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' ))); } catch (error) { map_game_apps = new Map(); }
        try { map_low_players_apps = new Map(JSON.parse(FS.readFileSync( './public/json/map_low_players_apps.json', 'utf-8' ))); } catch (error) { map_low_players_apps = new Map(); }
        let date_today             = new Date();
        let promises               = [];

        for (let [key, value] of map_game_apps.entries())
        {
            let date_steam_count_updated = value.date_steam_count_updated ? new Date( value.date_steam_count_updated ) : new Date('2000-01-01T01:00:00');
            let time_elapsed             = ( date_today - date_steam_count_updated ) / ( 60 * 60 * 1000 );
            let hours_1                  = 1;
            let bool_is_today            = (date_today.toDateString() == new Date(value.subreddit_date_updated).toDateString());
            // if the map hasn't been updated in the past 24 hours, do so
            if ( bool_is_today == false )
            {
                promises.push
                ( 
                    new Promise((resolve) => 
                    {
                        STEAM.getGamePlayers(key).then(player_count =>
                        {
                            // write_log(`App(${key}) \t Name: ${value.name} \t Player_count: ${player_count} \t date_steam_count_updated: ${date_steam_count_updated} `);
                            write_log(`STEAM.getGamePlayers( ${key} ).then(( ${active_player_count} ))`);
                            value.steam_count = player_count;
                            value.date_steam_count_updated = date_today.toISOString();
                            resolve([key, value]);
                        })
                        .catch((error) => 
                        {
                            value.steam_count = 0;
                            value.date_steam_count_updated = date_today.toISOString();
                            write_log(`STEAM.getGamePlayers( ${key} ) ERROR`);
                            write_log(error.message);
                            resolve([key, value]);
                        });
                    })
                );
            }
            // if the game hasn't been updated in the past 1 hour, do so
            else if ( time_elapsed >= hours_1 )
            {
                promises.push
                (
                    new Promise((resolve) => 
                    {
                        STEAM.getGamePlayers(key).then(active_player_count =>
                        {
                            write_log(`STEAM.getGamePlayers( ${key} ).then(( ${active_player_count} ))`);
                            if (active_player_count > value.steam_count) value.steam_count = active_player_count;
                            value.date_steam_count_updated = date_today.toISOString();
                            resolve([key, value]);
                        })
                        .catch((error) =>
                        {
                            write_log(`update_steam_count( ${key} ) ERROR`);
                            write_log(error.message);
                            resolve([key, value]);
                        });
                    })
                );
            }
        }

        Promise.all( promises )
        .then((results) => 
        {
            write_log('promises.length: ' + promises.length);
            if (!results.length > 0)
            {
                write_log('resolve1');
                resolve();
            }
            else
            {
                for (let [key, value] of results)
                {
                    if (value.steam_count >= 100)
                    {
                        value.date_steam_count_updated = date_updated;
                        map_game_apps.set(key, value);
                    }
                    else
                    {
                        write_log(value.name + ' low player count: ' + value.steam_count);
                        map_game_apps.delete(key);
                        map_low_players_apps.set(key, '');
                    }
                };
    
                // sort probably not needed here
                map_game_apps = new Map([...map_game_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                FS.writeFile( './public/json/map_game_apps.json', JSON.stringify( map_game_apps, mapAwareReplacer ), 'utf-8', ( error1 ) =>
                {
                    if ( error1 )
                    {
                        throw error1;
                    }
                    else
                    {
                        write_log( 'map_game_apps.json updated!' );

                        map_low_players_apps = new Map([...map_low_players_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending
                        FS.writeFile( './public/json/map_low_players_apps.json', JSON.stringify( map_low_players_apps, mapAwareReplacer), 'utf-8', ( error2 ) =>
                        {
                            if ( error2 )
                            {
                                throw error2;
                            }
                            else
                            {
                                write_log( 'map_low_players_apps.json updated!' );
                                resolve();
                            }
                        });
                    }
                });
            }
        })
        .catch((error) =>
        {
            reject(error);
        });
    });
    return promise_update_steam_count;
}


// Update Game App 
app.patch( '/update_game_app', async ( request, response ) => 
{ 
    try 
    {  
        let map_game_apps = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' )));  
 
        let app_id = parseInt(request.body.app_id); 
        let new_reddit_name = request.body.new_reddit_name; 
 
        let new_value = map_game_apps.get(app_id); 
        new_value.subreddit_name = String(new_reddit_name).toLocaleLowerCase(); 
        // write_log(new_value); 
 
        // get subreddit 
        REDDIT_API.getSubreddit(new_reddit_name).subscribers.then((subreddit_count) => 
        { 
            // save game_app with reddit 
            new_value.subreddit_count = parseInt(subreddit_count); 
 
            map_game_apps.set(app_id, new_value); 
            FS.writeFile( './public/json/map_game_apps.json', JSON.stringify( map_game_apps, mapAwareReplacer ), 'utf-8', ( error1 ) => 
            { 
                if ( error1 ) 
                { 
                    throw error1; 
                } 
                else 
                { 
                    // write_log( 'map_game_apps.json has been updated!' ); 
                    response.json([true, parseInt(subreddit_count)]); 
                } 
            }); 
        }); 
    } 
    catch (error) 
    {  
        write_log('/app.patch( \'/update_game_app\')'); 
        write_log(error); 
        response.json([false, error]); 
    } 
}); 
 
// update reddit_count for map_game_apps 
function update_reddit_count()  
{ 
    let promise_update_reddit_count = new Promise((resolve, reject) => 
    { 
        // schedule next update 
        let date_today = new Date(); 
        // start update 
        let map_game_apps; 
        try { map_game_apps        = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' ))); } catch (error) { map_game_apps = new Map(); } 
        let promises               = []; 
 
        for (let [key, value] of map_game_apps.entries()) 
        { 
            let subreddit_date_updated = value.subreddit_date_updated ? new Date( value.subreddit_date_updated ) : new Date('2000-01-01T01:00:00'); 
            let time_elapsed           = ( date_today - subreddit_date_updated ) / ( 60 * 60 * 1000 ); 
            let hours_1 = 1; 
            let bool_is_today = (date_today.toDateString() == new Date(value.subreddit_date_updated).toDateString()); 
            // if the game hasn't been updated for the current day 
            // write_log(`if ( bool_is_today ) = ${bool_is_today} |\t${value.name}`); 
            if ( bool_is_today == false ) 
            { 
                promises.push 
                ( 
                    new Promise((resolve) =>  
                    { 
                        REDDIT_API.getSubreddit(value.subreddit_name).active_user_count.then((active_user_count) => 
                        { 
                            write_log(`getSubreddit( ${value.subreddit_name} ).then(( ${active_user_count} ))`); 
                            value.subreddit_count = active_user_count; 
                            value.subreddit_date_updated = date_today.toISOString(); 
                            resolve([key, value]); 
                        }) 
                        .catch((error) => 
                        { 
                            value.subreddit_count = 0; 
                            value.subreddit_date_updated = date_today.toISOString(); 
                            write_log('update_reddit_count ERROR'); 
                            write_log(error.message); 
                            resolve([key, value]); 
                        }); 
                    }) 
                ); 
            } 
            // if the game hasn't been updated in the past 1 hour, do so 
            else if ( time_elapsed >= hours_1 ) 
            { 
                promises.push 
                ( 
                    new Promise((resolve) =>  
                    { 
                        REDDIT_API.getSubreddit(value.subreddit_name).active_user_count.then((active_user_count) => 
                        { 
                            write_log(`getSubreddit( ${value.subreddit_name} ).then(( ${active_user_count} ))`); 
                            if (active_user_count > value.subreddit_count) value.subreddit_count = active_user_count; 
                            value.subreddit_date_updated = date_today.toISOString(); 
                            resolve([key, value]); 
                        }) 
                        .catch((error) => 
                        { 
                            write_log(`update_reddit_count ( ${value.subreddit_name} ) ERROR`); 
                            write_log(error.message); 
                            resolve([key, value]); 
                        }); 
                    }) 
                ); 
            } 
        } 
 
        Promise.all( promises ) 
        .then((results) =>  
        { 
            write_log('promises.length: ' + promises.length); 
            if (!results.length > 0) 
            { 
                write_log('resolve1_______1'); 
                resolve(); 
            } 
            else 
            { 
                // write_log(results); 
                for (let [key, value] of results) 
                { 
                    map_game_apps.set(key, value); 
                } 
                // sort probably not needed here 
                map_game_apps = new Map([...map_game_apps].sort((int_key1, int_key2) => int_key1[0] - int_key2[0])); // Sort by integer key value ascending 
                FS.writeFile( './public/json/map_game_apps.json', JSON.stringify( map_game_apps, mapAwareReplacer ), 'utf-8', ( error1 ) => 
                { 
                    if ( error1 ) 
                    { 
                        throw error1; 
                    } 
                    else 
                    { 
                        write_log( 'map_game_apps.json has been updated!' ); 
                        resolve(); 
                    } 
                }); 
            } 
        }) 
        .catch((error) => 
        { 
            reject(error); 
        }); 
    }); 
    return promise_update_reddit_count; 
} 

function get_subreddit(game_name) 
{ 
    // await new Promise(resolve => setTimeout(resolve, 500)); // wait .5 seconds 
    let promise_get_reddit = new Promise((resolve, reject) => 
    { 
        try 
        { 
            game_name = game_name.replace(/ /g, '+'); 
            game_name = game_name.replace(/[^\w/d\+1/]*/g, ''); 
            JSEARCH.google(`reddit+${game_name}`, 0 , ((array_link_results) => 
            { 
                write_log('Game: '+ game_name); 
                // write_log('google results:'); 
                // write_log(array_link_results); // for Google results  
                let array_reddit_subs = []; 
                let reddit_blacklist =  
                [ 
                    'all', 
                    'patientgamers', 
                    'games', 
                    'pcgaming', 
                    'mmorpg', 
                    'shouldibuythisgame', 
                    'indiegames', 
                    'eshopperreviews', 
                    'steam', 
                    'gaming', 
                    'incremental_games', 
                    'simracing', 
                    'xboxone', 
                    'xboxseriesx', 
                    'xbox', 
                    'ps2', 
                    'ps3', 
                    'ps4', 
                    'ps5', 
                    '3ds', 
                    'androidgaming', 
                    'iosgaming', 
                    'linux_gaming', 
                    'vitahacks', 
                    'xboxgamepass', 
                    'boardgames', 
                    'nintendoswitch', 
                    'truegaming', 
                    'gamedeals', 
                    'steamdeals', 
                    'videos', 
                    'realtimestrategy', 
                    'steamdeck', 
                    'gamingleaksandrumours', 
                    'outoftheloop', 
                    'gamephysics', 
                    'buildapc', 
                    'webgames', 
                    'hoggit', 
                    'computerwargames', 
                    'gamedev', 
                    'nextfuckinglevel', 
                    'sysadmin', 
                    'pcmasterrace', 
                    'perfectlycutscreams', 
                    'movies', 
                    'technology', 
                    'worldnews', 
                    'askreddit',
                    'amitheasshole',
                    'nostupidquestions',
                    'whitepeopletwitter',
                    'todayilearned',
                    'soccer',
                    'memes',
                    'anime'
                ]; 
             
                // grab possible reddit links 
                for (i = 0; i < array_link_results.length; i++) 
                { 
                    let string_link_result = array_link_results[i]  
                    // write_log(string_link_result); 
                    if (String(string_link_result).includes('www.reddit.com/r/')) 
                    { 
                        let string_link = String(string_link_result).substring(String(string_link_result).indexOf('/r/') + 3); 
                        // write_log('string_link 1: ' + string_link); 
                        string_link = String(string_link).split('/')[0]; 
                        // write_log('string_link 2: ' + string_link); 
 
                        // regexp matches any words characters including '_' & all digits 
                        let reg_exp = new RegExp(/[^\w\d]/g); 
 
                        // let result1 = reg_exp.test(string_link);            
                        // write_log(result1); 
                        // if the reddit link has not already been added, && is not in the blacklist && does not contain illegal characters 
                        if (!array_reddit_subs.includes(String(string_link).toLowerCase()) && !reddit_blacklist.includes(string_link.toLowerCase()) 
                        &&  !reg_exp.test(string_link))  
                        { 
                            // write_log('\t' + string_link + ' added'); 
                            array_reddit_subs.push(String(string_link).toLowerCase()); 
                        } 
                    } 
                } 
             
                write_log('Game related subs:') 
                write_log(array_reddit_subs); 
             
                if (array_reddit_subs.length == 0) 
                { 
                    reject('Zero game related subs'); 
                } 
                else 
                { 
                    // select the most popular one 
                    let promises = []; 
                     
                    // write_log('HELLO1'); 
 
 
                    array_reddit_subs.forEach( (subreddit_name) => 
                    { 
                        // write_log('HELLO: ' + subreddit_name); 
 
                        promises.push( new Promise((resolve, reject) =>  
                        {  
                            try 
                            { 
                                resolve(REDDIT_API.getSubreddit(subreddit_name).active_user_count); 
                                // resolve(REDDIT_API.getSubreddit(subreddit_name).subscribers); 
                            } 
                            catch (error) 
                            { 
                                reject(error); 
                            } 
                        }) 
                        .catch((error) => 
                        { 
                            write_log(`ERROR: array_reddit_subs.forEach( (${subreddit_name}) => `); 
                            write_log(`Subreddit: PRIVATE. Forbidden access.`) 
                            // write_log(error) 
                        })); 
                    }); 
                    // ======================================================================= SPAGHETTI ZONE 
 
                    // write_log('HELLO2'); 
 
                    // Once all subreddit counts have been found 
                    Promise.all( promises ).then((array_subreddit_count) => 
                    { 
                        // Loop to figure out the reddit sub with the highest sub_count 
                        let index1 = 0; 
                        let index1_of_highest = 0; 
                        let highest_sub_count = array_subreddit_count[0]; 
 
                        array_subreddit_count.forEach(sub_count => 
                        { 
                            // write_log('Sub: ' + array_reddit_subs[index1] + ', sub_count: ' + sub_count); 
                            if (sub_count > highest_sub_count) 
                            { 
                                highest_sub_count = sub_count; 
                                index1_of_highest = index1; 
                            } 
                            index1++; 
                        }); 
 
                        game_name = String(game_name.replace(/[+]/g, ' ')).toLowerCase(); 
                        // write_log('GAME_NAME: ' + game_name); 
                        // Compare how similar the found reddit sub names are to the game name 
                        let string_similarity_result = STRING_SIMILARITY.findBestMatch(game_name, array_reddit_subs); 
                        // write_log(string_similarity_result.ratings); 
                        // write_log(string_similarity_result.bestMatch.target); 
                        // write_log(string_similarity_result.bestMatch.rating); 
                        // write_log(string_similarity_result.bestMatchIndex); 
 
                        // if the reddit_sub bestMatch rating is least 35%, use that name 
                        // else use the reddit sub that is most popular 
                        if (string_similarity_result.bestMatch.rating >= 0.35) 
                        { 
                            // increase similarity for game rating if it includes the same numbers as the name 
                            // for example: 'left 4 dead 2' - 'l4d2' + (.6 points * matching numbers) = +1.2 to rating > 'left4Dead' + .3 points 
 
                            // write_log('BEFORE:'); 
                            // write_log(string_similarity_result); 
 
                            let game_numbers = game_name.split(/[^0-9]+/); 
                             
                            // remove empty elements 
                            game_numbers = game_numbers.filter(element => element !== '');  
                            // write_log(game_numbers); 
                            let index2 = 0; 
                            let bestMatch_index = 0; 
                            let bestMatch_rating = string_similarity_result.ratings[0].rating; 
                            let bestMatch_target = string_similarity_result.ratings[0].target; 
                            string_similarity_result.ratings.forEach((element) => 
                            { 
                                game_numbers.forEach((number) => 
                                { 
                                    if (element.target.includes(number)) 
                                    { 
                                        element.rating += 1; 
                                    } 
                                    else 
                                    { 
                                        element.rating -= 1; 
                                    } 
                                }); 
                             
                                if (bestMatch_rating < element.rating) 
                                { 
                                    bestMatch_index   = index2; 
                                    bestMatch_rating = element.rating; 
                                    bestMatch_target = element.target; 
                                } 
                                index2++; 
                            }); 
                            write_log('AFTER:'); 
                            write_log(string_similarity_result); 
                            // write_log('bestMatch_index: ' + bestMatch_index); 
                            // write_log('bestMatch_rating: ' + bestMatch_rating); 
                            // write_log('bestMatch_target: ' + bestMatch_target); 
                            resolve([bestMatch_target, array_subreddit_count[bestMatch_index]]); 
                            write_log('The subreddit with the best similarity match is: ' + bestMatch_target + ' (' +  string_similarity_result.bestMatch.rating.toFixed(3) + ')' + ', sub_count: ' + array_subreddit_count[bestMatch_index]); 
                        } 
                        else 
                        { 
                            resolve([array_reddit_subs[index1_of_highest], array_subreddit_count[index1_of_highest]]); 
                            write_log('The subreddit with the highest sub_count is: ' + array_reddit_subs[index1_of_highest] + ', sub_count: ' + array_subreddit_count[index1_of_highest]); 
                        } 
                    }); 
                } 
            })); 
        } 
        catch (error) 
        { 
            write_log(`Error: function get_subreddit(${game_name})`); 
            write_log(error); 
            reject(error);     
        } 
    }); 
    return promise_get_reddit; 
} 

function get_tags(app_id)
{
    // await new Promise(resolve => setTimeout(resolve, 500)); // wait .5 seconds
    let promise_get_tags = new Promise((resolve, reject) =>
    {
        try 
        {
            let targetURL = `https://store.steampowered.com/app/${app_id}/`;
            // AXIOS function to fetch HTML Markup from target URL 
            AXIOS.default.request({
                'url': targetURL,
                'method': 'post',
                'headers':
                    {
                        'Cookie': 'birthtime=283993201; mature_content=1;'
                    }
            }).then((response) => { 
                const body = response.data; 
                const $ = CHEERIO.load(body); // Load HTML data and initialize CHEERIO

                let tags         = $('.app_tag').text();
                let release_date = $('#genresAndManufacturer').text();
                write_log('release_date TEXT');
                // write_log(release_date);
                release_date = release_date.split('Release Date: ')[1];
                write_log(release_date);

                write_log(tags);
                tags = tags.split(/[,\n\t]+/);
                tags = tags.filter(element => element !== '+' && element !== '');

                write_log(tags);
                write_log(tags.length);
                resolve(tags);
            });
        }
        catch (error)
        {
            write_log(`Error: function get_tags(${app_id})`);
            write_log(error);
            reject(error);    
        }
    });
    return promise_get_tags;
}

function mapAwareReplacer(key, value)
{
    if (value instanceof Map && typeof value.toJSON !== "function") {
        return [...value.entries()]
    }
    return value
}

function write_log(text)
{
    let date_now = new Date();
    FS.appendFileSync(STRING_DIR_LOGS, `\n${date_now.toISOString().split('T')[0].replace(/-/g, '/') + ', ' + date_now.toLocaleTimeString() + ': \t' + text}`, 'utf-8');
}

function schedule_game_counts_update()
{
    write_log(`is_updating = ${is_updating}`);
    let date_today = new Date();
    let date_next  = new Date(date_today.setHours(date_today.getHours() + 1, 0, 0, 0));
    SCHEDULE.scheduleJob(date_next, schedule_game_counts_update);
    write_log('Next update scheduled for: ' + date_next.toLocaleString());
    if ( is_updating == false)
    {
        try
        {
            let map_game_apps          = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' )));
            let date_map_last_updated  = new Date(map_game_apps.values().next().value.date_steam_count_updated) ? new Date(map_game_apps.values().next().value.date_steam_count_updated) : new Date('2000-01-01T01:00:00');
            let bool_map_updated_today = (date_today.toDateString() == date_map_last_updated.toDateString());
            write_log(map_game_apps.values().next().value);    
            write_log('bool_map_updated_today: ' + bool_map_updated_today);
            // if the map hasn't been updated in the past 24 hours, do so
            if ( bool_map_updated_today == false )
            {
                steam_create_map();
            }
            else
            {
                // complete one after the other because conflicts of map_game_apps values happen otherwise
                update_steam_count().then(() =>
                {
                    update_reddit_count();
                });
            }
        }
        catch (error) 
        {
            steam_create_map();
        }
    }
}

// server listening to PORT 
// app.listen( process.env.PORT, '192.168.50.157', () =>
app.listen( process.env.PORT, () =>
{
    console.log('Listening on PORT: ' + process.env.PORT);
    write_log('Listening on PORT: ' + process.env.PORT);
    schedule_game_counts_update();
});
