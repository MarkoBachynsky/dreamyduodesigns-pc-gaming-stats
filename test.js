const FS = require( 'fs' );
const js = require('jsearch');

const cheerio = require('cheerio');
const axios = require('axios'); 

const googleTrends = require('google-trends-api');

var startTime, endTime;

// debuging / logging
let STRING_FOLDER_NAME = `logs/${new Date().toISOString().split('T')[0]}`;
// FS.mkdirSync(STRING_FOLDER_NAME);
FS.mkdirSync(STRING_FOLDER_NAME, { recursive: true });
FS.mkdirSync('public/json/google_trends', { recursive: true });

let ARRAY_FILE_NAMES = FS.readdirSync(STRING_FOLDER_NAME);
let STRING_FILE_NAME = `${ARRAY_FILE_NAMES.length}_debug`;
// Write data in 'Output.txt' .
let STRING_DIR_LOGS = `${STRING_FOLDER_NAME}/${STRING_FILE_NAME}.log`;

function start()
{
    startTime = new Date();
};

function end()
{
    endTime = new Date();
    var timeDiff = endTime - startTime; //in ms

    // get seconds 
    var seconds = Math.round(timeDiff);
    write_log(timeDiff + " ms");
}

// let date_yesterday = new Date('2023-02-14T19:11');
// let date_today     = new Date('2023-02-15T18:11');
// let hours = 24;

// let time_elapsed = (date_today - date_yesterday) / (60 * 60 * 1000)

// write_log(date_yesterday.toISOString());
// write_log(date_today.toISOString() +'\n');

// write_log(time_elapsed);
// write_log(hours);

// if (time_elapsed >= hours)
// {
//     write_log(true);
// }
// else
// {
//     write_log(false);
// }

// function getBinarySize(string) {
//     return Buffer.byteLength(string, 'utf8');
// }

// write_log('1: ' + getBinarySize(`appid:1593510,name:Furry Finder,player_count:3841,last_updated:2023-02-20T16:27:04.745Z`));
// write_log('2: ' + getBinarySize(JSON.stringify( map_thing, mapAwareReplacer)));

// let array = [1, 2, 3, 4, 5, 6]
// let array2 = [1, 2, 3, 4]
// let last_key = array2[array2.length - 1];

// loop1:
// for (let i = array.length - 1; i >= 0; i--)
// {
//     let valueAtIndex = array[i];

//     if (valueAtIndex > last_key)
//     {
//         array2.push(valueAtIndex);
//     }
//     else
//     {
//         break loop1;
//     }
// }
// array2.sort();

// ============================================================

// let obje = {};
// obje.name = 'Marko'
// obje.release_date =
// {
//     "coming_soon" : false,
//     "date" :"Nov 1, 2000"
// }

// let date = obje.release_date.date;

// write_log(date.split(', ')[1]);

// let item = false;

// let is_free = [];

// is_free.push(item);

// write_log(is_free);
// is_free.sort();

// write_log(Object.hasOwn(obje, 'name'));
// write_log(Object.hasOwn(obje, 'nam2'));

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target)
{
    let number = [];
    for (i = 0; i < nums.length; i++)
    {
        outer_loop:
        for (j = 0; j < nums.length; j++)
        {
            // write_log('nums[i]' + nums[i]);
            // write_log('nums[j]' + nums[j]);
            if ((nums[i] + nums[j]) === target && i != j)
            {
                return [i, j];
            }
        }
    }
};

function write_log(text)
{
    let date_now = new Date();
    FS.appendFileSync(STRING_DIR_LOGS, `\n${date_now.toISOString().split('T')[0].replace(/-/g, '/') + ', ' + date_now.toLocaleTimeString() + ': \t' + text}`, 'utf-8');
}

// let nums = [2,7,11,15];
// let target = 9;

// write_log(twoSum(nums, target));
async function main()
{

    let map_game_apps = new Map(JSON.parse(FS.readFileSync( './public/json/map_game_apps.json', 'utf-8' )));
    // let search_terms = ['Star Wars', 'Counter Strike', 'Left 4 Dead 2', 'Terraria', 'Star', 'LOTR'];
    // let array_of_search_terms = [];
    // let i = 0, j = 0;
    // let group = []
    // for (let [key, value] of map_game_apps.entries())
    // {
    //     // start();
    //     // replace all non-alphanumberic chars with space
    //     value.name = value.name.replace(/[^.'a-z0-9]/gi, ' ');
    //     // Replace multiple spaces with single space
    //     value.name = value.name.trim().split(/[\s,\t,\n]+/).join(' ');
    //     // group.push(value.name);

    //     if(!FS.existsSync(`./public/json/google_trends/${key}.json`))
    //     {
    //         await new Promise(resolve => setTimeout(resolve, 3000)); // wait 1.0 seconds

    //             write_log(value.name.length + '\t' + value.name);
    //             googleTrends.interestOverTime({keyword: value.name, geo: 'US'})
    //             .then((results) =>
    //             {
    //                 let json_results = JSON.parse(results);
    //                 // write_log(value.name);
    //                 // write_log(json_results.default.timelineData[0]);
    //                 write_log(json_results.default.timelineData[0]);
    //                 // write_log(json_results.default.averages);
                    
                    

    //                 // if data exists, write to file
    //                 if (!(json_results.default.timelineData[0] === undefined))
    //                 {
    //                     FS.writeFile( `./public/json/google_trends/${key}.json`, JSON.stringify( json_results), 'utf-8', ( error1 ) =>
    //                     {
    //                         if ( error1 )
    //                         {
    //                             write_log( `${key}.json failed, NOT SAVED.` );
    //                             throw error1;
    //                         }
    //                         else
    //                         {
    //                             write_log( `${key}.json SAVED.` );
    //                             // end();
    //                         }
    //                     } );
    //                 }
    //                 else
    //                 {
    //                     write_log( `${key}.json NOT SAVED, no data.` );
    //                 }
    //             })
    //         .catch((error) =>
    //         {
    //             write_log(error);
    //         });
            
    //     }
        
    //     // if ((group.length % 5) === 0) 
    //     // {
    //     //     array_of_search_terms.push(group);
    //     //     group = [];
    //     // }
    //     // if (array_of_search_terms.length === 6) break;
    // }
    
    // write_log(array_of_search_terms);
    let array_of_search_terms = ['Marvel', 'Star Wars'];
    
    for (let search_terms of array_of_search_terms)
    {
        await new Promise(resolve => setTimeout(resolve, 500)); // wait 2.4 seconds
        // let search_term = 'Might and Magic Heroes VII Trial by Fire';
        googleTrends.interestOverTime({keyword: search_terms, geo: 'US'})   
        .then((results) =>
        {
            write_log(search_terms);
            let json_results = JSON.parse(results);
            // write_log(json_results.default);
            write_log(json_results.default.timelineData[0]);
            // write_log(json_results.default.timelineData[1]);
            // write_log(json_results.default.timelineData[2]);
            // write_log(json_results.default.timelineData[3]);
            write_log(json_results.default.averages);
        })
        .catch((error) =>
        {
            write_log('error');
            write_log(error);
        })
    }
    



}


main();